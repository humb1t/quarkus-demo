package io.humb1t.quarkusdemo;

import java.util.Set;
import java.util.HashSet;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

@Path("/resources")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Resource {

	private Set<Payload> payloads = new HashSet<>();

	public Resource() {
		payloads.add(new Payload("First", 1));
		payloads.add(new Payload("Second", 3));
	}

	@GET
	public Set<Payload> list() {
		return payloads;
	}

	@POST
	public Set<Payload> add(Payload payload) {
		payloads.add(payload);
		return payloads;
	}

	@DELETE
	public Set<Payload> delete(Payload payload) {
		payloads.remove(payload);
		return payloads;
	}
}
