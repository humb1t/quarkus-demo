package io.humb1t.quarkusdemo;

import java.util.Objects;

public class Payload {
	private String name;
	private int quantity;

	public Payload() {
	}

	public Payload(String name, int quantity) {
		this.name = name;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Payload)) {
			return false;
		}
		Payload other = (Payload) obj;
		return Objects.equals(other.name, this.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.name);
	}
}
