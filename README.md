```bash
docker build -f src/main/docker/Dockerfile.multistage -t quarkus-demo .
```

```bash
docker run -i --rm -p 8080:8080 quarkus-demo
```
